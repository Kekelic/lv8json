package com.example.lv8json;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private List<Product> products= new ArrayList<>();

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View productView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_product, parent, false);
        return new ProductViewHolder(productView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = products.get(position);
        holder.setIvProduct(product.image_link);
        holder.setTvName("Name: "+product.name);
        holder.setTvPrice("Price: "+product.price);
        holder.setTvRating("Price "+product.rating);
        holder.setTvDescription(product.description.replaceAll("\n"," "));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void addProducts(List<Product> products){
        this.products.clear();
        this.products.addAll(products);
        notifyDataSetChanged();
    }

}
