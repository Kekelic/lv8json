package com.example.lv8json;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RecyclerAdapter adapter;
    private EditText etBrand;
    private Button bSearch;
    private Call<List<Product>> apiCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupRecycler();
        etBrand = (EditText) findViewById(R.id.etbrend);
        setupButton();
    }

    private void setupRecycler(){
        recycler = findViewById(R.id.recycleView);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerAdapter();
        recycler.setAdapter(adapter);
    }

    private void setupButton(){
        bSearch = (Button) findViewById(R.id.bSearch);
        bSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpApiCall(etBrand.getText().toString());
            }
        });

    }

    private void setUpApiCall(String brand){
        Call<List<Product>> apiCall = NetworkUtils.getApiInterface().getProducts(brand);
        apiCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                if(response.isSuccessful() && (response.body() !=  null)){
                    if (response.body().size()==0){
                        Toast.makeText(MainActivity.this, "Rezultati pretraživanja nisu pronađeni.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        showProduct(response.body());
                        etBrand.getText().clear();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showProduct(List<Product> products){
        adapter.addProducts(products);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(apiCall != null)
            apiCall.cancel();
    }
}