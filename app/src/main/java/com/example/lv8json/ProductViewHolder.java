package com.example.lv8json;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class ProductViewHolder extends RecyclerView.ViewHolder {
    private TextView tvName, tvPrice, tvRating, tvDescription;
    private ImageView ivProduct;


    public ProductViewHolder(@NonNull View itemView) {
        super(itemView);

        tvName = itemView.findViewById(R.id.tvName);
        tvPrice = itemView.findViewById(R.id.tvPrice);
        tvRating = itemView.findViewById(R.id.tvRating);
        tvDescription = itemView.findViewById(R.id.tvDescription);
        ivProduct = itemView.findViewById(R.id.ivProduct);
    }

    public void setTvName(String name) {
        tvName.setText(name);
    }

    public void setTvPrice(String price) {
        tvPrice.setText(price);
    }

    public void setTvRating(String rating) {
        tvRating.setText(rating);
    }

    public void setTvDescription(String description) {
        tvDescription.setText(description);
    }

    public void setIvProduct(String imageLink) {
        Picasso.get().load(imageLink).into(ivProduct);
    }
}
