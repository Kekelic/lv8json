package com.example.lv8json;

public class Product {
    public String name;
    public String price;
    public String image_link;
    public String description;
    public String rating;
}
