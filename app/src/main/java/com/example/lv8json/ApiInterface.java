package com.example.lv8json;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("api/v1/products.json?brand={}")
    Call<List<Product>> getProducts(@Query("brand") String brand);
}
